import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { Loc } from '../../models/location';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { GlobalService } from 'src/app/services/global.service';
import { ActionSheetController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  // initialize for maps
  marker: Loc;
  lat = -6.1881882;
  lng = 106.7925963;
  location: any;

  // initialize for upload foto
  imageUrl = '';
  base64Image: string;
  imageToUpload: string;

  // initialize for form
  reportForm: FormGroup;

  // initialize variable
  currentRegion: any;
  currentRole: String;
  userUid: any;

  // initialize loading
  isLoading: any;

  // initialize variable for reportdata
  reportData: any = [];

  @ViewChild('map') mapElement: ElementRef;
  constructor(private storage: Storage,
              private geoloc: Geolocation,
              private camera: Camera,
              private actionSheetController: ActionSheetController,
              private globalService: GlobalService,
              private afFirestore: AngularFirestore) { 
    this.marker = new Loc(this.lat, this.lng);
  }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    // get current role
    this.storage.get('session').then((value) => {
      console.log(value)
      if (value != null && value !== '') {
        this.currentRole = value.role;
        console.log(this.currentRole);

        // if role is user then initialize form
        if(this.currentRole === 'user') {
          this.initializeForm();
        }
      }
    });

    this.storage.get('token').then((token) => {
      if (token != null && token !== '') {
        this.userUid = token;
      }
    });

    this.afFirestore.collection('pandemic').snapshotChanges().subscribe(res => {
      console.log(res)
      let tmp = [];
      res.forEach(task => {
        tmp.push({ key: task.payload.doc['id'], ...task.payload.doc.data});
      });
      console.log(tmp);
      this.reportData = tmp
    })
  }

  private initializeForm() {
    this.reportForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      age: new FormControl(null, Validators.compose([Validators.required, Validators.pattern('[0-9]*')])),
      address: new FormControl(null, Validators.required),
      gender: new FormControl(null, Validators.required),
      latitude: new FormControl(null),
      longitude: new FormControl(null)
    });
  }

  // set to marker
  onSetMarker(event: any) {
    this.marker = new Loc(event.coords.lat, event.coords.lng);
    this.lat = this.marker.lat;
    this.lng = this.marker.lng;
  }

  // get current location
  onLocate() {
    this.globalService.presentLoading().then(resp => {
      this.geoloc.getCurrentPosition()
        .then(
          currLocation => {
            this.location = new Loc(currLocation.coords.latitude, currLocation.coords.longitude);
            this.marker = new Loc(currLocation.coords.latitude, currLocation.coords.longitude);
            this.lat = this.location.lat;
            this.lng = this.location.lng;
          }
        )
        .catch(
          error => {
            console.log(error);
          }
        );
      this.globalService.dismissLoading();
    });
  }

  // take photo
  onTakePhoto() {
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true
    };

    this.camera.getPicture(options).
      then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,' + imageData;
      },
        (err) => {
          this.base64Image = 'error';
        });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Image source',
      buttons: [{
        text: 'Take Photo',
        handler: () => {
          this.onTakePhoto();
        }
      }]
    });
    await actionSheet.present();
  }

  // select on change by region
  optionsFn(event) {
    this.currentRegion = event.target.value;
  }

  // submit to firebase
  onSubmit(){
    console.log(this.reportForm.value)
    this.afFirestore.collection('pandemic').add({
      name: this.reportForm.value.name,
      age: this.reportForm.value.age,
      address: this.reportForm.value.address,
      photo: 'this.base64Image',
      gender: this.reportForm.value.gender,
      location: {'lat' : this.reportForm.value.latitude, 'lng' : this.reportForm.value.longitude}
    })
  }

}
