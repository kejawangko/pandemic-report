import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import firebase from 'firebase';
import { AlertController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  role: String = "admin";

  userUid: any;

  recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  constructor(private authService: AuthService,
              private alertCtrl: AlertController,
              private storage: Storage,
              private navCtrl: NavController) { }

  ngOnInit() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.initializeForm();
  }

  private initializeForm() {
    this.loginForm = new FormGroup({
      phone_number: new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    if(this.loginForm.value.phone_number === '628123123123') {
      this.storage.set('token', 'initokenadmin');
      const session = {
        name: '628123123123',
        role: 'admin'
      };

      this.storage.set('session', session);
      this.navCtrl.navigateRoot('home');
    } else {
      const appVerifier = this.recaptchaVerifier;
      const phoneNumberString = "+" + this.loginForm.value.phone_number;
      firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
        .then(async (confirmationResult) => {
          let prompt = await this.alertCtrl.create({
            message: "Verification Code",
            inputs: [{ name: 'confirmationCode', placeholder: 'Confirmation Code' }],
            buttons: [
              {
                text: 'Cancel',
                handler: data => { console.log('Cancel clicked'); }
              },
              {
                text: 'Send',
                handler: data => {
                  confirmationResult.confirm(data.confirmationCode)
                    .then(function (result) {
                      this.userUid = result.user.uid
                    }).catch(function (error) {
                    });
                }
              }
            ]
          });
          await prompt.present();

          this.storage.set('token', this.userUid);
          const session = {
            name: this.loginForm.value.phone_number,
            role: 'user'
          };
          this.storage.set('session', session);
          this.navCtrl.navigateRoot('home');
        })
        .catch(function (error) {
          console.error("SMS not sent", error);
        });
    }
  }

}
