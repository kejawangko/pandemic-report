import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from './services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Dashboard', url: 'home', icon: 'home' },
    { title: 'Report', url: 'report', icon: 'file-tray-full' },
  ];
  constructor(private platform: Platform,
              private storage: Storage,
              private navCtrl: NavController,
              private authService: AuthService) {
    this.initializeApp();
  }

  isLogin = 0;
  nameLogin = '';
  initializeApp() {
    // ini buat live
    this.storage.get('token').then((value) => {
      if (value != null && value !== '') {
        this.isLogin = 1;
        this.storage.get('session').then((value) => {
          if (value != null && value !== '') {
            this.nameLogin = value.name;
            this.navCtrl.navigateRoot('home');
          }
        });
      } else {
        this.navCtrl.navigateRoot('/login');
      }
    });
  }

  logout() {
    this.authService.logout();
  }
}
