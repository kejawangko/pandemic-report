import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {
    isLoading: any;

    constructor(private loadingCtrl: LoadingController) { }

    async presentLoading() {
        return await this.loadingCtrl.create({
            message: 'Please wait...',
            duration: 5000,
        }).then(a => {
            a.present();
            //   if (!this.isLoading) {
            //     a.dismiss().then(() => console.log('abort presenting'));
            //   }
        });
    }

    async dismissLoading() {
        // this.isLoading = false;
        return await this.loadingCtrl.dismiss();
    }
}
