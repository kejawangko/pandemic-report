import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ToastController, NavController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    errorWording = "Silakan coba lagi";
    isLoggedIn = false;
    token: any;

    constructor(
        private http: HttpClient,
        private storage: Storage,
        private navCtrl: NavController,
        private toastController: ToastController
    ) { }

    login(data) {
        console.log(data);
        let role = "employee";
        this.http.get("assets/data/auth.txt").subscribe((result: any) => {
            for (let i = 0; i < result.length; i++) {
                if (data.phone_number === result[i].phone_number) {
                    this.storage.set('token', result[i].token);
                    role = result[i].role;
                    const session = {
                        name: result[i].name,
                        role: result[i].role
                    };
                    this.storage.set('session', session);
                    break;
                }

            }

            this.navCtrl.navigateRoot('home');
        })
    }

    getUser() {
        return new Promise(async (resolve, reject) => {
            this.storage.get('token').then((value) => {
                if (value != null && value !== '') {
                    this.storage.get('session').then((value) => {
                        if (value != null && value !== '') {
                            resolve(value);
                        }
                    });
                }
            });
        });
    }

    logout() {
        const session = {
            name: '',
            role: ''
        };
        this.storage.set('session', session);
        this.storage.set('token', '');
        this.navCtrl.navigateRoot('/login');
    }
}
